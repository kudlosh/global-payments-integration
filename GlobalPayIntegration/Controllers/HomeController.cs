﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GlobalPayIntegration.Models;
using GlobalPayments.Api;
using GlobalPayments.Api.Services;
using GlobalPayments.Api.Entities;
using Newtonsoft.Json;
using Microsoft.Net.Http.Headers;
using System.Net.Mime;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.IO;

namespace GlobalPayIntegration.Controllers
{
    public class HomeController : Controller
    {
        private const string testMerchantId = "realexsandbox";
        private const string testSharedSecret = "Po8lRRT67a";
        private const string testServiceUrl = "https://test.realexpayments.com/epage-remote.cgi";

        // does not work
        private const string sandboxMerchantId = "dev554545396273006406";
        private const string sandboxSharedSecret = "lAiTMkw6a6";
        private const string sandboxAccountId = null;
        private const string sandboxServiceUrl = "https://pay.sandbox.realexpayments.com/pay";

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SetupPayment()
        {
            // configure client, request and HPP settings
            var service = new HostedService(new GpEcomConfig()
            {
                MerchantId = sandboxMerchantId,                
                SharedSecret = sandboxSharedSecret,
                ServiceUrl = sandboxServiceUrl,
                HostedPaymentConfig = new HostedPaymentConfig
                {
                    Version = "2"
                },
            });

            // Add 3D Secure 2 Mandatory and Recommended Fields
            var hostedPaymentData = new HostedPaymentData
            {
                CustomerEmail = "james.mason@example.com",
                CustomerPhoneMobile = "44|07123456789",
                AddressesMatch = false,
                ReturnUrl = "https://globalpayintegration.conveyor.cloud/Home/PaymentResult",
            };

            var billingAddress = new Address
            {
                StreetAddress1 = "Flat 123",
                StreetAddress2 = "House 456",
                StreetAddress3 = "Unit 4",
                City = "Halifax",
                PostalCode = "W5 9HR",
                Country = "826"
            };

            var shippingAddress = new Address
            {
                StreetAddress1 = "Apartment 825",
                StreetAddress2 = "Complex 741",
                StreetAddress3 = "House 963",
                City = "Chicago",
                State = "IL",
                PostalCode = "50001",
                Country = "840",
            };

            try
            {
                var response = service.Charge(19.99m)
                   .WithCurrency("EUR")
                   .WithHostedPaymentData(hostedPaymentData)
                   .WithAddress(billingAddress, AddressType.Billing)
                   .WithAddress(shippingAddress, AddressType.Shipping)
                   .Serialize();

                return Content(response, "application/json");
                
                // TODO: pass the HPP request JSON to the JavaScript, iOS or Android Library
            }

            catch (ApiException exce)
            {
                // TODO: Add your error handling here
            }

            throw new Exception("blbý");
        }

        [HttpPost("Home/PaymentResult")]
        public async Task<IActionResult> PaymentResultPost()
        {
            // configure client settings
            var service = new HostedService(new GpEcomConfig
            {
                MerchantId = sandboxMerchantId,
                SharedSecret = sandboxSharedSecret,
                ServiceUrl = sandboxServiceUrl,
            });

            var responseJson = JsonConvert.SerializeObject(Request.Form.ToDictionary(key => key.Key, value => value.Value.ToString()));

            try
            {
                // create the response object from the response JSON
                Transaction response = service.ParseResponse(responseJson, false);
                var orderId = response.OrderId; // GTI5Yxb0SumL_TkDMCAxQA
                var responseCode = response.ResponseCode; // 00
                var responseMessage = response.ResponseMessage; // [ test system ] Authorised
                var responseValues = response.ResponseValues; // get values accessible by key
                var fraudFilterResult = responseValues["HPP_FRAUDFILTER_RESULT"]; // PASS

                // TODO: update your application and display transaction outcome to the customer

                if (responseCode != "00")
                {
                    ViewBag.ErrorMsg = $"{responseCode} - {responseMessage}";
                    return PartialView("PaymentUnsuccessful");
                }
            }
            catch (ApiException exce)
            {
                // TODO: add your error handling here
                ViewBag.ErrorMsg = exce.Message;
                return PartialView("PaymentUnsuccessful");
            }
            catch (Exception ex)
            {
                ViewBag["errorMsg"] = "Error when processing payment.";
                return PartialView("PaymentUnsuccessful");
            }

            return PartialView("PaymentSuccessful");
        }

        [HttpPost("Home/PaymentStatus")]
        public IActionResult PaymentStatusPost()
        {
            string de = "bug";
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Pay()
        {
            string body;
            using (var reader = new StreamReader(Request.Body))
            {
                body = await reader.ReadToEndAsync();
            }

            throw new NotImplementedException();
        }
    }
}
